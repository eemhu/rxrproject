# RXR Project
## VR Obstacle avoiding robot simulation made in Unity

## Details
Simple obstacle avoiding robot that uses three raycasts to emulate front, left and right-sided ultrasonic sensors.
Can be used in Oculus PCVR (requires Touch controllers)

Place markers using Right Trigger, place obstacles using X and remove obstacles using Right Grip.
You can also disable the VR_Player and use FirstPerson-AIO instead for a desktop experience.

## Requirements
* Unity 2019.4.13f1 (64-bit)
* Oculus PCVR headset and Oculus Touch controllers (VR Mode)
