﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RXR_RobotMovement : MonoBehaviour
{

    public GameObject ENDLINE;
    private Vector3 STARTLINE;

    private Vector3 ENDPOS;

    private bool END_REACHED;

    public float INITIAL_ROTATION = 45f;

    public float MAX_ANGLE_TO_FORWARD = 5f;

    public bool ON_END_REACHED_RESET = false;

    public bool ENABLE_LOGGING = false;

    public bool ENABLE_RAYCAST_DEBUG = true;

    public float MAX_DISTANCE_OF_RAYCAST = 1f;

    enum RXR_Direction {
        Left,
        Right
    }

    // Start is called before the first frame update
    void Start()
    {
        STARTLINE = this.transform.position;
        ENDPOS = ENDLINE.transform.position;

        this.transform.Rotate(0.0f, INITIAL_ROTATION, 0.0f, Space.Self);
    }

    void FixedUpdate() {
        
        // Cast sensor-type ray
        CastRay(-this.transform.right, MAX_DISTANCE_OF_RAYCAST, ENABLE_RAYCAST_DEBUG, Color.red); // forward
        CastRay(this.transform.forward, MAX_DISTANCE_OF_RAYCAST, ENABLE_RAYCAST_DEBUG, Color.blue); // right
        CastRay(-this.transform.forward, MAX_DISTANCE_OF_RAYCAST, ENABLE_RAYCAST_DEBUG, Color.cyan);// left

        if (!END_REACHED) {
            this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(ENDPOS.x, this.transform.position.y, ENDPOS.z), Time.deltaTime * 1f);

            float angleBetweenForwardAndRobotFace = Vector3.Angle(transform.forward, Vector3.forward);

            if (angleBetweenForwardAndRobotFace > MAX_ANGLE_TO_FORWARD) {
                float angleBetweenForwardAndRobotRight = Vector3.Angle(transform.right, Vector3.forward);
                float angleBetweenForwardAndRobotLeft = Vector3.Angle(-transform.right, Vector3.forward);

                if (angleBetweenForwardAndRobotRight > angleBetweenForwardAndRobotLeft) {
                    // turn left
                    turn(RXR_Direction.Left, 10f);
                }
                else {
                    // turn right
                    turn(RXR_Direction.Right, 10f);
                }
            }
        }

        if (ENABLE_LOGGING) print(Vector3.Angle(transform.forward, Vector3.forward));
    }

    void CastRay(Vector3 towards, float maxDistance, bool debug, Color debugColor) {
        Vector3 face = this.transform.position; // this.transform.TransformDirection(this.transform.position);
        Vector3 fwd = towards; // this.transform.TransformDirection(towards);

        RaycastHit hit;
        if (debug) Debug.DrawRay(face, fwd, debugColor, 0.025f);
        if (Physics.Raycast(face, fwd, out hit, maxDistance) && hit.collider.CompareTag("obstacle")) {
            if (debug) {
                string sensorName = "";
                if (fwd == -this.transform.right) {
                    sensorName = "frontal";
                }
                else if (fwd == this.transform.forward) {
                    sensorName = "right";
                }
                else if (fwd == -this.transform.forward) {
                    sensorName = "left";
                }
                else {
                    sensorName = "unknown";
                }

                print("RaycastHit with " + hit.collider.tag + " from sensor " + sensorName);
            }
        }
    }

    void CastDebugRay(Vector3 from, Vector3 towards) {
        Debug.DrawRay(this.transform.TransformVector(from), this.transform.TransformVector(towards), Color.yellow, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.name == ENDLINE.name) {
            print("Collision with end line !!");
            if (ON_END_REACHED_RESET) {
                this.transform.position = STARTLINE;
                this.transform.Rotate(0.0f, INITIAL_ROTATION, 0.0f, Space.Self);
            }
            else {
                END_REACHED = true;
            }
        }
    }

    void turn(RXR_Direction direction, float deg) {
        switch (direction){
            case RXR_Direction.Left: this.transform.Rotate(0.0f, -1f * deg * Time.deltaTime, 0.0f, Space.Self); break; // left
            case RXR_Direction.Right: this.transform.Rotate(0.0f, 1f * deg * Time.deltaTime, 0.0f, Space.Self); break; // right
            default: break;
        }
    }

    float checkDistance(Vector3 robotFace, GameObject obstacle) {
        return Vector3.Distance(robotFace, obstacle.transform.position);
    }
}
