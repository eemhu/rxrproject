﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RXR_ObstaclePickUp : MonoBehaviour
{
    public bool canHold = true; // are the hands of the player empty
    public GameObject manipulatedObject; // currently carried gameobject
    
    public Transform guide; // player's hand

    public float rotationManipulationMultiplier = 1f; // how fast the x/y/z keys rotate the in-hand object
    
    public bool neutralizeVelocitiesOnPickup = true;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            if (canHold) {
                // pickup
                Pickup();
            }
            else {
                // drop
                Drop();
            }
        }

        // If an item is being held, change it position to match the player's "hand"
        if (!canHold && manipulatedObject) {
            manipulatedObject.transform.position = guide.position;

            if (Input.GetKey(KeyCode.R)) {
                print("In-hand object was reset to a neutral position.");
                manipulatedObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                manipulatedObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                manipulatedObject.transform.rotation = new Quaternion(0, 0, 0, 0);
            }
            else if (Input.GetKey(KeyCode.X)) {
                manipulatedObject.transform.Rotate(rotationManipulationMultiplier * 0.1f, 0, 0, Space.Self);
            }
            else if (Input.GetKey(KeyCode.Y)) {
                manipulatedObject.transform.Rotate(0, rotationManipulationMultiplier * 0.1f, 0, Space.Self);
            }
            else if (Input.GetKey(KeyCode.Z)) {
                manipulatedObject.transform.Rotate(0, 0, rotationManipulationMultiplier * 0.1f, Space.Self);
            }
            else if (Input.GetKey(KeyCode.G)) {
                Destroy(manipulatedObject);
                manipulatedObject = null;
                canHold = true;
            }
        }
    }

    // When the player enters the obstacle trigger
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "obstacle") {
            if (!manipulatedObject) {
                manipulatedObject = other.gameObject;
            }
        }
    }

    // When the player exits the obstacle trigger
    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "obstacle") {
            if (canHold) {
                manipulatedObject = null;
            }
        }
    }

    void Pickup() {
        if (!manipulatedObject) return;

        manipulatedObject.transform.SetParent(guide);
        manipulatedObject.GetComponent<Rigidbody>().useGravity = false;
        //manipulatedObject.transform.localRotation = transform.rotation;
        manipulatedObject.transform.position = guide.position;

        if (neutralizeVelocitiesOnPickup) {
            manipulatedObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            manipulatedObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
        
        canHold = false;
    }

    void Drop() {
        if (!manipulatedObject) return;

        manipulatedObject.GetComponent<Rigidbody>().useGravity = true;
        manipulatedObject = null;
        guide.GetChild(0).parent = null;

        canHold = true;
    }
}
