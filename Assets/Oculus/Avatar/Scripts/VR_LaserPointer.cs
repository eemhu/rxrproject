﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OVR;

public class VR_LaserPointer : MonoBehaviour
{
    public LineRenderer laserLineRenderer;
    public float laserWidth = 0.1f;
    public float laserMaxLength = 10f;
    OvrAvatar ovrAvatar;

    public GameObject marker1;
    public GameObject marker2;
    public GameObject spawnablePrecedent;
    public GameObject floor;

    //OVRCameraRig cameraRig;
    //public Text text;

    private float timeAfterLastPress = 1.1f;

    private RaycastHit lastHit;

    void Start()
    {
        Vector3[] initLaserPositions = new Vector3[2] { Vector3.zero, Vector3.zero };
        laserLineRenderer.SetPositions(initLaserPositions);
        laserLineRenderer.startWidth = laserWidth;
        laserLineRenderer.endWidth = laserWidth;
        //cameraRig = FindObjectOfType<OVRCameraRig>();
        ovrAvatar = FindObjectOfType<OvrAvatar>();
    }
    void Update()
    {
        OVRInput.Update();
        timeAfterLastPress += Time.deltaTime;

        lastHit = ShootLaserFromTargetPosition(/*cameraRig.rightHandAnchor.localPosition*/
        ovrAvatar.HandRight.transform.position, ovrAvatar.HandRight.transform.forward, laserMaxLength);
        //text.text = "laser pointer triggered"; //debug text
        laserLineRenderer.enabled = true;
        

        if (OVRInput.Get(OVRInput.RawButton.RIndexTrigger) && timeAfterLastPress > 1.0f)
        {

            spawnMarker(lastHit);
            timeAfterLastPress = 0.0f;

        }
        else if (OVRInput.Get(OVRInput.RawButton.RHandTrigger) && timeAfterLastPress > 1.0f)
        {

            if (lastHit.collider.CompareTag("obstacle"))
            {
                Destroy(lastHit.collider.gameObject);
                timeAfterLastPress = 0.0f;
            }

        }
        else if (OVRInput.Get(OVRInput.RawButton.X) && timeAfterLastPress > 1.0f)
        {
            GameObject cloned = Instantiate(spawnablePrecedent);
            cloned.SetActive(true);
            cloned.transform.position = new Vector3(lastHit.point.x, floor.transform.position.y + 0.1f, lastHit.point.z);
            //cloned.GetComponent<Rigidbody>().useGravity = false;
            timeAfterLastPress = 0.0f;
        }
        else
        {
           // text.text = "";
            //laserLineRenderer.enabled = false;
        }
    }
    RaycastHit ShootLaserFromTargetPosition(Vector3 targetPosition, Vector3 direction, float length)
    {
        Ray ray = new Ray(targetPosition, direction);
        Vector3 endPosition = targetPosition + (length * direction);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, length))
        {
            endPosition = raycastHit.point;
            laserLineRenderer.SetPosition(0, targetPosition);
            laserLineRenderer.SetPosition(1, endPosition);
            return raycastHit;

            
        }
        laserLineRenderer.SetPosition(0, targetPosition);
        laserLineRenderer.SetPosition(1, endPosition);
        return new RaycastHit();
        
    }

    void spawnMarker(RaycastHit raycastHit)
    {
        Debug.Log(raycastHit.transform.name);
        if (raycastHit.transform.name == "Floor")
        {
            if (!marker1.activeSelf)
            {
                marker1.transform.position = raycastHit.point;
                marker1.SetActive(true);
            }
            else if (!marker2.activeSelf)
            {
                marker2.transform.position = raycastHit.point;
                marker2.SetActive(true);
            }
            else
            {
                marker2.SetActive(false);
                marker1.transform.position = marker2.transform.position;
                marker2.transform.position = raycastHit.point;
                marker2.SetActive(true);
            }
        }
    }
}
