﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RXR_SpawnMarker : MonoBehaviour
{

    // TODO remove hit markers, do not allow spawning markers too close to each other (spawning a target too close to the robot causes it to not move; also a problem when two markers are too close to each other and the robot spawns on one of them)

    public GameObject marker1;
    public GameObject marker2;
    public Camera playerCam;
    public GameObject avatar;

    // Update is called once per frame
    void Update()
    {
        //OVRInput.Update();

        if (Input.GetMouseButtonDown(1))
        {
            //spawnMarker();
        }

        //if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger)) {
        //    spawnMarker();
        //}
    }

    void spawnMarker()
    {

        

        RaycastHit hit;
        if(Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hit))
        {
            Debug.Log(hit.transform.name);
            if (hit.transform.name == "Floor")
            {
                if (!marker1.activeSelf)
                {
                    marker1.transform.position = hit.point;
                    marker1.SetActive(true);
                }
                else if (!marker2.activeSelf)
                {
                    marker2.transform.position = hit.point;
                    marker2.SetActive(true);
                }
                else
                {
                    marker2.SetActive(false);
                    marker1.transform.position = marker2.transform.position;
                    marker2.transform.position = hit.point;
                    marker2.SetActive(true);
                }
            }
        }

    }
}
