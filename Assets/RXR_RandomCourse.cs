﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RXR_RandomCourse : MonoBehaviour
{

    public GameObject z;
    public GameObject zx;
    public GameObject x;

    public GameObject[] obstacles = new GameObject[5];

    float timeSinceLastObstacleCourse = 1.1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.K) && Input.GetKey(KeyCode.LeftShift)) {
            for (int i = 0; i < obstacles.Length; i++) {
                if (obstacles[i]) Destroy(obstacles[i]);
            }
        }
        else if (Input.GetKey(KeyCode.K) && timeSinceLastObstacleCourse > 0.3f) {
            //float zArea = Mathf.Abs(z.transform.position.z - zx.transform.position.z);
            //float xArea = Mathf.Abs(zx.transform.position.x - x.transform.position.x);
            float yVal = z.transform.position.y;
            Random rnd = new Random();

            for (int i = 0; i < obstacles.Length; i++) {
                GameObject obstacle = Instantiate(z);
                obstacle.transform.position = new Vector3(Random.Range(zx.transform.position.x, x.transform.position.x), yVal, Random.Range(z.transform.position.z, zx.transform.position.z));
                if (obstacles[i]) Destroy(obstacles[i]);
                obstacles[i] = obstacle;
            }
            timeSinceLastObstacleCourse = 0.0f;
        }
        else {
            timeSinceLastObstacleCourse += Time.deltaTime;
        }
    }
}
