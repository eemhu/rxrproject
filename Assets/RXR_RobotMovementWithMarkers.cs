﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RXR_RobotMovementWithMarkers : MonoBehaviour
{

    public GameObject startMarker;
    public GameObject endMarker;

    private bool facingTarget = false;
    private bool positionReset = false; // whether or not the robot's position has just been reset to that of startMarker or STARTLINE

    private bool adjustingPosition = false;
    private float timeSinceLastAdjustment = 0.0f;

    public GameObject ENDLINE;
    private Vector3 STARTLINE;

    private Vector3 ENDPOS;

    private bool END_REACHED;

    public float INITIAL_ROTATION = 45f;

    public float MAX_ANGLE_TO_TARGET = 5f;

    public bool ON_END_REACHED_RESET = false;

    public bool ENABLE_LOGGING = false;

    public bool ENABLE_RAYCAST_DEBUG = true;

    public float MAX_DISTANCE_OF_RAYCAST = 1f;

    enum RXR_Direction
    {
        Left,
        Right
    }

    // Start is called before the first frame update
    void Start()
    {
        STARTLINE = this.transform.position;
        ENDPOS = ENDLINE.transform.position;

        this.transform.Rotate(0.0f, INITIAL_ROTATION, 0.0f, Space.Self);
    }

    void FixedUpdate()
    {

        if (Input.GetKeyDown(KeyCode.Tab)) {
            this.transform.position = STARTLINE;
            this.transform.Rotate(0.0f, 0.0f, 0.0f, Space.Self);
            facingTarget = false;
        }

        // Cast sensor-type ray
        Vector3[] forwardHit = CastRay(-this.transform.right, MAX_DISTANCE_OF_RAYCAST, ENABLE_RAYCAST_DEBUG, Color.red); // forward
        Vector3[] rightHit = CastRay(this.transform.forward, MAX_DISTANCE_OF_RAYCAST, ENABLE_RAYCAST_DEBUG, Color.blue); // right
        Vector3[] leftHit = CastRay(-this.transform.forward, MAX_DISTANCE_OF_RAYCAST, ENABLE_RAYCAST_DEBUG, Color.cyan);// left

        if (!END_REACHED)
        {

            float currentAngle = 0; // this is updated in the following if-else statement and checked later

            // if the endmarker position has changed, update ENDPOS and check whether or not rotation is necessary (the facingTarget flag)
            if (endMarker.activeSelf && ENDPOS != endMarker.transform.position)
            {
                ENDPOS = endMarker.transform.position;
                facingTarget = false;
            }
            else if (positionReset) // if the robot has just respawned at a marker, check if rotation is necessary
            {
                facingTarget = false;
                positionReset = false;
            }

            // Obstacle avoidance logic start
            if (forwardHit[0] == Vector3.one && leftHit[0] == Vector3.zero) {
                turn(RXR_Direction.Left, 90f, 50f); // was 50f
                facingTarget = false;
                adjustingPosition = true;
                timeSinceLastAdjustment = 0.0f;
            }
            else if (forwardHit[0] == Vector3.one && rightHit[0] == Vector3.zero) {
                turn(RXR_Direction.Right, 90f, 50f); // was 50f
                facingTarget = false;
                adjustingPosition = true;
                timeSinceLastAdjustment = 0.0f;
            }
            else if (adjustingPosition) {
                timeSinceLastAdjustment += Time.deltaTime;

                if (timeSinceLastAdjustment > 0.5f) {
                    forwardHit = CastRay(-this.transform.right, MAX_DISTANCE_OF_RAYCAST, ENABLE_RAYCAST_DEBUG, Color.red);
                    this.transform.position = Vector3.MoveTowards(this.transform.position, this.transform.TransformPoint(forwardHit[1]) - this.transform.right, Time.deltaTime * 45f); //was 60f
                    adjustingPosition = false;
                    timeSinceLastAdjustment = 0.0f;
                }
                
            }
            // Obstacle avoidance logic end
            
            if (!adjustingPosition) {
                 // if the robot has turned to face the target (the angle is small enough), move towards it
                    if (facingTarget)
                    {
                        this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(ENDPOS.x, this.transform.position.y, ENDPOS.z), Time.deltaTime * 1f);
                    }
                    else
                    {
                        // if there is an end marker, turn towards it
                        if (endMarker.activeSelf)
                        {
                            Vector3 target = endMarker.transform.position - this.transform.position;
                            currentAngle = Vector3.Angle(transform.right, target); // the angle between the robot and the target

                            if (Mathf.Abs(currentAngle - 180) > MAX_ANGLE_TO_TARGET) // for some reason, this is necessary here but not in the else-branch
                            {
                                facingTarget = false;
                                float angleBetweenTargetAndRobotRight = Vector3.Angle(transform.forward, target);
                                float angleBetweenTargetAndRobotLeft = Vector3.Angle(-transform.forward, target);

                                if (angleBetweenTargetAndRobotRight > angleBetweenTargetAndRobotLeft)
                                {
                                    // turn left
                                    turn(RXR_Direction.Left, 30f);
                                }
                                else
                                {
                                    // turn right
                                    turn(RXR_Direction.Right, 30f);
                                }
                            }
                            else
                            {
                                //   this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(ENDPOS.x, this.transform.position.y, ENDPOS.z), Time.deltaTime * 1f);
                                facingTarget = true;
                            }
                        }
                        else // if there is no end marker, turn the robot so that transform.forward approximates Vector3.forward (0, 0, 1)
                        {
                            currentAngle = Vector3.Angle(transform.forward, Vector3.forward); // the angle between the robot's Z axis and (0, 0, 1)

                            if (currentAngle > MAX_ANGLE_TO_TARGET)
                            {
                                facingTarget = false;
                                float angleBetweenForwardAndRobotRight = Vector3.Angle(transform.right, Vector3.forward);
                                float angleBetweenForwardAndRobotLeft = Vector3.Angle(-transform.right, Vector3.forward);

                                if (angleBetweenForwardAndRobotRight > angleBetweenForwardAndRobotLeft)
                                {
                                    // turn left
                                    turn(RXR_Direction.Left, 30f);
                                }
                                else
                                {
                                    // turn right
                                    turn(RXR_Direction.Right, 30f);
                                }
                            }
                            else
                            {
                                facingTarget = true;
                                // this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(ENDPOS.x, this.transform.position.y, ENDPOS.z), Time.deltaTime * 1f);
                            }
                        }
                    }
                }
        
            }
           
        if (ENABLE_LOGGING) print(Vector3.Angle(transform.forward, Vector3.forward)); // TODO add something more meaningful here

    }

    Vector3[] CastRay(Vector3 towards, float maxDistance, bool debug, Color debugColor)
    {
        Vector3 face = this.transform.position; // this.transform.TransformDirection(this.transform.position);
        Vector3 fwd = towards; // this.transform.TransformDirection(towards);

        RaycastHit hit;
        if (debug) Debug.DrawRay(face, fwd, debugColor, 0.025f);
        if (Physics.Raycast(face, fwd, out hit, maxDistance) && hit.collider.CompareTag("obstacle"))
        {
            if (debug)
            {
                string sensorName = "";
                if (fwd == -this.transform.right)
                {
                    sensorName = "frontal";
                }
                else if (fwd == this.transform.forward)
                {
                    sensorName = "right";
                }
                else if (fwd == -this.transform.forward)
                {
                    sensorName = "left";
                }
                else
                {
                    sensorName = "unknown";
                }

                print("RaycastHit with " + hit.collider.tag + " from sensor " + sensorName);

            }

            return new Vector3[]{Vector3.one, hit.point};   // true, hit point
        }
        return new Vector3[]{Vector3.zero, Vector3.zero}; // false
    }

    void CastDebugRay(Vector3 from, Vector3 towards)
    {
        Debug.DrawRay(this.transform.TransformVector(from), this.transform.TransformVector(towards), Color.yellow, 1f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == endMarker.name)
        {
            if (ON_END_REACHED_RESET)
            {
                this.transform.position = new Vector3(startMarker.transform.position.x, this.transform.position.y, startMarker.transform.position.z);
                positionReset = true;
            }
            else
            {
                END_REACHED = true;
                // TODO have the markers swap polarities and have the robot turn 180 degrees and make a new trip? (not a necessary feature)
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.name == ENDLINE.name)
        {
            print("Collision with end line !!");
            if (ON_END_REACHED_RESET)
            {
                if (startMarker.activeSelf)
                {
                    if (endMarker.activeSelf)
                    {
                        ENDPOS = endMarker.transform.position;
                    }
                    this.transform.position = new Vector3(startMarker.transform.position.x, this.transform.position.y, startMarker.transform.position.z);
                    positionReset = true;
                }
                else
                {
                    this.transform.position = STARTLINE;
                    this.transform.Rotate(0.0f, INITIAL_ROTATION, 0.0f, Space.Self);
                    positionReset = true;
                }
            }
            else
            {
                END_REACHED = true;
            }
        }

    }

    void turn(RXR_Direction direction, float deg, float multi = 1f)
    {
        switch (direction)
        {
            case RXR_Direction.Left: this.transform.Rotate(0.0f, -multi * deg * Time.deltaTime, 0.0f, Space.Self); break; // left
            case RXR_Direction.Right: this.transform.Rotate(0.0f, multi * deg * Time.deltaTime, 0.0f, Space.Self); break; // right
            default: break;
        }
    }

    float checkDistance(Vector3 robotFace, GameObject obstacle)
    {
        return Vector3.Distance(robotFace, obstacle.transform.position);
    }
}
